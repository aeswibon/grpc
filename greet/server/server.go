package main

import (
	"context"
	"log"
	"net"

	greetpb "gitlab.com/aeswibon/grpc/proto"
	"google.golang.org/grpc"
)

type greetServer struct {
	greetpb.UnimplementedGreetServiceServer
}

func (server *greetServer) Greet(ctx context.Context, req *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	log.Printf("Greet function was invoked with %v", req)
	firstName := req.GetGreeting().GetFirstName()
	result := "Hello " + firstName
	res := &greetpb.GreetResponse{
		Result: result,
	}
	return res, nil
}

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err) // log.Fatalf will print the error and exit the program
	}
	s := grpc.NewServer()
	greetpb.RegisterGreetServiceServer(s, &greetServer{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err) // log.Fatalf will print the error and exit the program
	}
}
